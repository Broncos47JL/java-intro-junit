package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LightSaberTest {

    @Test
    public void lightSaberShouldInstantiateWhenPassedAString(){
        LightSaber myLightSaber = new LightSaber(4815162342l);
        Long test = myLightSaber.getJediSerialNumber();
        assertEquals((Long)4815162342L, test);
    }

    @Test
    public void setChargeShouldSetChargeAndAllowItToBeReturnedUnderGetCharge(){
        LightSaber myLightSaber = new LightSaber(4815162342L);
        myLightSaber.setCharge(100.0f);
        float test = myLightSaber.getCharge();
        assertEquals(100.0f, test);
    }

    @Test
    public void setChargeShouldSetColorAndAllowItToBeReturnedUnderGetColor(){
        LightSaber myLightSaber = new LightSaber(4815162342L);
        myLightSaber.setColor("blue");
        String test = myLightSaber.getColor();
        assertEquals("blue", test);
    }

    @Test
    public void useShouldDepleteChargeByTenForEachHour(){
        LightSaber myLightSaber = new LightSaber(4815162342L);
        myLightSaber.use(120.0f);
        float test = myLightSaber.getRemainingMinutes();
        assertEquals(480.0f,test);
    }

    @Test
    public void rechargeShouldIncreaseChargeToOneHundred(){
        LightSaber myLightSaber = new LightSaber(4815162342L);
        myLightSaber.use(120.0f);
        myLightSaber.recharge();
        float test = myLightSaber.getRemainingMinutes();
        assertEquals(600.0f,test);
    }

    @Test
    public void testSix(){
        LightSaber myLightSaber = new LightSaber(3691215l);
        Long test = myLightSaber.getJediSerialNumber();
        assertEquals((Long)3691215L, test);
    }

    @Test
    public void testSeven(){
        LightSaber myLightSaber = new LightSaber(123456789l);
        Long test = myLightSaber.getJediSerialNumber();
        assertEquals((Long)123456789L, test);
    }

    @Test
    public void testEight(){
        LightSaber myLightSaber = new LightSaber(1029384756l);
        Long test = myLightSaber.getJediSerialNumber();
        assertEquals((Long)1029384756L, test);
    }
}
